CREATE TABLE Pokemon (
  pid int(11) NOT NULL,
  pname varchar(255) NOT NULL,
  type varchar(45) DEFAULT NULL,
  speed int(11) DEFAULT NULL,
  mega tinyint(4) DEFAULT NULL,
  bst int(11) DEFAULT NULL,
  PRIMARY KEY (pid));

CREATE TABLE Type (
  type varchar(45) NOT NULL,
  typeid int(11) DEFAULT NULL,
  PRIMARY KEY (type));

CREATE TABLE Resistance (
  typeid int(11) DEFAULT NULL,
  resist varchar(45) NOT NULL,
  PRIMARY KEY (resist),
  FOREIGN KEY (typeid) REFERENCES pokemon_type (typeid));

CREATE TABLE Vulnerable (
  typeid int(11) DEFAULT NULL,
  vulnerable varchar(45) NOT NULL,
  PRIMARY KEY (vulnerable),
  FOREIGN KEY (typeid) REFERENCES pokemon_type (typeid));

CREATE TABLE Weak (
  typeid int(11) DEFAULT NULL,
  weakness varchar(45) NOT NULL,
  PRIMARY KEY (weakness),
  FOREIGN KEY (typeid) REFERENCES pokemon_type (typeid));;

